cmake_minimum_required (VERSION 2.8.11)
project("imgui")

set(IMGUI_SOURCES
    imgui.cpp
    imgui_demo.cpp
    imgui_draw.cpp
    imgui_widgets.cpp)

set(IMGUI_HEADERS
    imconfig.h
    imgui.h
    imgui_internal.h
    imstb_rectpack.h
    imstb_textedit.h
    imstb_truetype.h)

add_library(imgui STATIC ${IMGUI_SOURCES})
set_target_properties(imgui PROPERTIES POSITION_INDEPENDENT_CODE ON)
set_target_properties(imgui PROPERTIES COMPILE_PDB_NAME "imgui")

if(MSVC)
    target_compile_options(imgui PRIVATE "/wd4244")
    target_compile_options(imgui PRIVATE "/wd4267")
    target_compile_options(imgui PRIVATE "/wd4307")
endif()

install(FILES ${IMGUI_HEADERS}
        DESTINATION include/imgui)
install(TARGETS imgui
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)
if(MSVC)
    install(FILES ${PROJECT_BINARY_DIR}/imgui.pdb
            DESTINATION lib
            CONFIGURATIONS Debug RelWithDebInfo)
endif()
